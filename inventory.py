#!/usr/bin/env python3

import os
import sys
import json
import pprint
import argparse
import hcl

# Define global variable
CONFIG = dict()

def parse_arguments():
  '''
  Parse command line arguments
  '''
  parser = argparse.ArgumentParser(description='Create dynamic inventory for ansible')
  parser.add_argument('--list', dest='LIST', action='store_true')
  parser.add_argument('--host', dest='HOST', action='store')
  parser.add_argument('-e', '--environment', dest='ENVIRONMENT', action='store', help='Environment must be available in terraform/environments directory:')
  args = parser.parse_args()
  return args

def process_arguments(args):
  '''
  process the arguments and set config where required
  '''
  global CONFIG
  # Set default value to environment variable for use as dynamic inventory
  CONFIG['environment'] = args.ENVIRONMENT or os.environ['ENV']
  CONFIG['host'] = args.HOST
  CONFIG['list'] = args.LIST
  return

def get_full_inventory():
  inventory = {
    'master': {
      'hosts': [],
      'vars': {},
      'children': []
    },
    'worker': {
      'hosts': [],
      'vars': {},
      'children': []
    },
    'all': {
      'children': [
        'master',
        'worker'
      ]
    }
  }
  hcl_file = "/code/environments/{}/env.hcl".format(CONFIG['environment'])
  with open(hcl_file, 'r') as fh:
    obj = hcl.load(fh)
    vms = obj["locals"]["vms"]

    # Loop over vms
    for (key,value) in vms.items():
      if value == "local.master_vm":
        inventory['master']['hosts'].append(key)
      elif value == "local.worker_vm":
        inventory['worker']['hosts'].append(key)

  return(inventory)

def get_host_vars(host):
  '''
  Specific host vars not implemented
  '''
  return (inventory)

def main():
  '''
  One function to rule them all! :D
  '''
  inventory = {}
  global CONFIG
  process_arguments(parse_arguments())

  if CONFIG['host']:
    inventory = get_host_vars(CONFIG['host'])
  else:
    inventory = get_full_inventory()

  print (json.dumps(inventory, indent=4, sort_keys=True))

if __name__ == '__main__':
  main(