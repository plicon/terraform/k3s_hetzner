#!/bin/bash
# Simple script to setup rancher on the cluster

helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
helm repo add jetstack https://charts.jetstack.io
helm repo update
sleep 5
echo "Create namespaces ..."
kubectl create namespace cattle-system
kubectl create namespace cert-manager
sleep 2
echo "Create required CRDS for cert-manager"
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.0/cert-manager.crds.yaml
sleep 2
echo "Helm install cert manager"
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v0.15.0
sleep 15
echo "Helm install rancher"
helm install rancher rancher-stable/rancher \
  --namespace cattle-system \
  --set hostname=rancher2.hetzner.plicon.nl \
  --set ingress.tls.source=letsEncrypt \
  --set letsEncrypt.email=patrick@plicon.nl

# Check status
  kubectl -n cattle-system rollout status deploy/rancher
