# Set the variable value in *.tfvars file
#------------------------------------------------------
# GENERAL
#------------------------------------------------------
variable "cluster_id" {}
variable "project_id" {}
variable "domain" {
  default = "plicon.nl"
}
variable "cloudflare_zone_id" {
  default = "0"
}
variable "cloudflare_argo_cert_key_file" {
  default = "~/.cloudflared/cert.pem"
}
variable "cf_argo_tunnel" {}