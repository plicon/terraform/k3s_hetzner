#--------------------------------------------------------
# Setup cluster for NGINX INGRESS with CLOUDFLARE ARGO Sidecar container
#
locals {
  tunnel_secret_name = "cf-argo-tunnel-secret"
}
resource "rancher2_namespace" "ingress-nginx" {
  name          = "ingress-nginx"
  project_id    = var.project_id
}

resource "rancher2_secret" "cf-argo-tunnel-secret" {
  name          = local.tunnel_secret_name
  description   = "CloudFlare ARGO Tunnel Secret"
  project_id    = var.project_id
  namespace_id  = rancher2_namespace.ingress-nginx.id
  data          = {
    "cert.pem"  = base64encode(file(pathexpand(var.cloudflare_argo_cert_key_file)))
  }
}

# Install Ingress Nginx with ARG-Sidecar Container
resource "rancher2_app_v2" "ingress-nginx" {
  cluster_id    = var.cluster_id
  project_id    = var.project_id
  name          = "ingress-nginx"
  namespace     = rancher2_namespace.ingress-nginx.id
  repo_name     = "ingress-nginx"
  chart_name    = "ingress-nginx"
  chart_version = "3.34.0"
  values        = templatefile(
                    "${path.module}/values/ingress-nginx_values.tpl",
                    {
                      hostname      = "${var.cf_argo_tunnel}.${var.domain}"
                      tunnel_secret = local.tunnel_secret_name
                    }
                )
}