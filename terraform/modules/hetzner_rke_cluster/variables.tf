# Set the variable value in *.tfvars file
#------------------------------------------------------
# GENERAL
#------------------------------------------------------

# or using the -var="hcloud_token=..." CLI option
variable "environment" {
  default = "my_env"
}
variable "cluster" {
  default = "my_cluster"
}
variable "subdomain" {
  default = "my"
}
variable "domain" {
  default = "domain.nl"
}
variable "hostname_prefix" {
  default = "k8s"
}
variable "custom_tags" {
  type = map
  default = {
    "Application" = "k3s"
    "Environment" = "my_env"
  }
}

#------------------------------------------------------
# CLOUDFLARE DOMAIN components
#------------------------------------------------------
variable "cloudflare_email" {
  default = "mail@domain.nl"
}
variable "cloudflare_api_token" {
  default = "my_super_secret_api_token"
}
variable "cloudflare_zone_id" {
  default = "1234567890987654321"
}
#------------------------------------------------------
# HCLOUD infra components
#------------------------------------------------------
variable "hcloud_token" {
  default = "!my_super_secret_token!"
}
variable "location" {
  default = "near_to_me"
}
variable "image_name" {
  default = "ubuntu-18.04"
}
variable "kube_config" {
  default     = "config"
  description = "Relative to ~/.kube"
}
variable "network" {
  default = "my_network"
}
variable "network_zone" {
  default = "my_network_zone"
}
variable "ip_range" {
  default = "192.168.0.0/16"
}
variable "ip_subnet" {
  default = "192.168.1.0/24"
}
variable "ip_address" {
  default = "192.168.1."
}
variable "num_master" {
  default = "1"
}
variable "volume_master" {
  default = "0"
}
variable "num_worker" {
  default = "3"
}

variable "volume_worker" {
  default = "0"
}
variable "master_type" {
  default = "cx11"
}
variable "worker_type" {
  default = "cx11"
}

#------------------------------------------------------
# Ansible
#------------------------------------------------------
variable "ansible_user" {
    default = "ansible"
}
variable "ansible_extra_opts" {
    default = ""
}
variable "ansible_extra_vars" {
    default = ""
}
variable "ansible_playbook" {
    default = "main.yaml"
}
variable "ansible_key_name" {
    default = "terraform-ansible-public-key"
}
variable "ssh_key_public_path" {
    default = "~/.ssh/id_rsa.pub"
}
variable "ssh_key_private_path" {
    default = "~/.ssh/id_rsa"
}
#------------------------------------------------------
# Rancher
#------------------------------------------------------
variable "rancher_api_url" {
  default = "https://rancher.plicon.nl/v3"
}
variable "rancher_access_key" {
  default = "username"
}
variable "rancher_secret_key" {
  default = "password"
}
#------------------------------------------------------
# Rancher RKE
#------------------------------------------------------
variable "network_policy" {
  default = true
}
variable "cni_plugin" {
  default = "canal"
}
variable "cni_interface" {
  default = "eth1"
}

#------------------------------------------------------
# RANCHER
#------------------------------------------------------
variable "rke_template" {
  default = "hcloud_rke"
}
variable "master_roles" {
  default = "hcloud_rke"
}

variable "worker_roles" {
  default = "hcloud_rke"
}
#------------------------------------------------------
# AWS
#------------------------------------------------------
variable "aws_access_key_id" {
  default = "my_access_key"
}
variable "aws_secret_access_key" {
  default = "super_secret_Access_key"
}
