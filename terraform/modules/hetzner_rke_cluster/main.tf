#####################################
# Create master node(s)
resource "hcloud_server" "master" {
  count       = var.num_master
  name        = format("%s-master%02d", var.hostname_prefix,count.index +1)
  image       = var.image_name
  server_type = var.master_type
  location    = "nbg1"

  labels = merge(
    var.custom_tags,
    tomap (
      {
        "Role" = "master",
        "Name" = format("%s-master%02d", var.hostname_prefix,count.index +1)
      }
    )
  )

  user_data = <<-EOF
    #cloud-config
    manage_etc_hosts: true
    preserve_hostname: false
    hostname: "${format("%s-master%02d", var.hostname_prefix, count.index +1)}"
    fqdn: "${format("%s-master%02d", var.hostname_prefix, count.index +1)}.${var.subdomain}.${var.domain}"
    users:
      - name: "${var.ansible_user}"
        shell: /bin/bash
        sudo: ALL=(ALL) NOPASSWD:ALL
        ssh_authorized_keys:
          - "${file(var.ssh_key_public_path)}"
  EOF
}

resource "hcloud_volume" "volume-master" {
  count       = var.volume_master > 0 ? var.num_master : 0
  name        = format("%s-volume-master%02d", var.hostname_prefix,count.index +1)
  location    = "nbg1"
  size        = var.volume_master
}
resource "hcloud_volume_attachment" "master" {
  count     = var.volume_master > 0 ? var.num_master : 0
  volume_id = hcloud_volume.volume-master[count.index].id
  server_id = hcloud_server.master[count.index].id
  automount = true
}

# Create worker node(s)
resource "hcloud_server" "worker" {
  count       = var.num_worker
  name        = format("%s-worker%02d", var.hostname_prefix,count.index +1)
  image       = var.image_name
  server_type = var.worker_type
  location    = "nbg1"

  labels = merge(
    var.custom_tags,
    tomap (
      {
      "Role" = "worker",
      "Name" = format("%s-worker%02d", var.hostname_prefix,count.index +1)
      }
    )
  )

  user_data = <<-EOF
    #cloud-config
    manage_etc_hosts: true
    preserve_hostname: false
    hostname: "${format("%s-worker%02d", var.hostname_prefix, count.index +1)}"
    fqdn: "${format("%s-worker%02d", var.hostname_prefix, count.index +1)}.${var.subdomain}.${var.domain}"
    users:
      - name: "${var.ansible_user}"
        shell: /bin/bash
        sudo: ALL=(ALL) NOPASSWD:ALL
        ssh_authorized_keys:
          - "${file(var.ssh_key_public_path)}"
  EOF
}
resource "hcloud_volume" "volume-worker" {
  count       = var.volume_worker > 0 ? var.num_worker : 0
  name        = format("%s-volume-worker%02d", var.hostname_prefix,count.index +1)
  location    = "nbg1"
  size        = var.volume_worker
}
resource "hcloud_volume_attachment" "worker" {
  count     = var.volume_worker > 0 ? var.num_worker : 0
  volume_id = hcloud_volume.volume-worker[count.index].id
  server_id = hcloud_server.worker[count.index].id
  automount = true
}
####################################
# Create network
resource "hcloud_network" "network" {
  name      = var.network
  ip_range  = var.ip_range
  labels    = var.custom_tags
}
# Add subnet to network
resource "hcloud_network_subnet" "subnet" {
  network_id    = hcloud_network.network.id
  type          = "server"
  network_zone  = var.network_zone
  ip_range      = var.ip_subnet
}
# Bind network to nodes
resource "hcloud_server_network" "master_network" {
  count       = var.num_master
  server_id   = hcloud_server.master[count.index].id
  subnet_id   = hcloud_network_subnet.subnet.id
}
resource "hcloud_server_network" "worker_network" {
  count       = var.num_worker
  server_id   = hcloud_server.worker[count.index].id
  subnet_id   = hcloud_network_subnet.subnet.id
}
