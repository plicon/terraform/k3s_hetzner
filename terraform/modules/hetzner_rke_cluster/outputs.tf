output "subdomain" {
  value = var.subdomain
}
output "domain" {
  value = var.domain
}
output "kube_config" {
  value = var.kube_config
}

output "cluster_kubeconfig" {
  value = rancher2_cluster_sync.cluster.kube_config
  sensitive = true
}

output "cluster_id" {
  value = rancher2_cluster_sync.cluster.id
}

output "rancher_api_server_url" {
  depends_on  = [ rancher2_cluster_sync.cluster ]
  value       = yamldecode(rancher2_cluster_sync.cluster.kube_config).clusters[0].cluster.server
}
output "rancher_api_token" {
  value     = yamldecode(rancher2_cluster_sync.cluster.kube_config).users[0].user.token
  sensitive = true
}