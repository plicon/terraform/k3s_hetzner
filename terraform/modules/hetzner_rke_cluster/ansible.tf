###############################################
# Run ansible-playbook on the created machines
locals {
  ansible_dir         = "${path.module}/ansible"
  ansible_extra_vars = var.ansible_extra_vars != "" ? "--extra-vars '${var.ansible_extra_vars}'" : ""
}

#data "archive_file" "ansible_trigger_group_vars" {
#  type          = "zip"
#  source_dir    = "${local.ansible_dir}/group_vars/"
#  output_path   = "ansible_group_vars.zip"
#}

resource "null_resource" "ansible" {
  triggers = {
    trigger_num_master = var.num_master
    trigger_num_worker = var.num_worker
#    always_run = timestamp()
#    trigger_group_vars        = data.archive_file.ansible_trigger_group_vars.output_md5
#    trigger_roles_requrements = file("${local.ansible_dir}/roles/requrements.yaml")
#    trigger_roles_requrements = file("${local.ansible_dir}/collections/requrements.yaml")
#    trigger_secrets_vault     = file("${local.ansible_dir}/secrets/all/vault.yaml")
#    trigger_playbook_site     = file("${local.ansible_dir}/site.yaml")
  }
  provisioner "local-exec" {
    command = <<EOT
      rm -rf ansible
      git clone git@gitlab.com:plicon/ansible-playbooks/hetzner_k3s.git ansible
    EOT
    working_dir = path.module
  }

  provisioner "local-exec" {
    command = <<EOT
      ansible-galaxy install -r roles/requirements.yaml -p roles --force
      ansible-galaxy collection install -r collections/requirements.yaml -p collections --force
    EOT
    working_dir = local.ansible_dir
  }

  provisioner "local-exec" {
    environment = {
        HCLOUD_TOKEN = var.hcloud_token
    }
    command = <<EOT
        ansible-playbook ./${var.ansible_playbook} -i inventory_hcloud.yaml -v ${var.ansible_extra_opts} ${local.ansible_extra_vars}
    EOT
    working_dir = local.ansible_dir
  }
  depends_on = [hcloud_server.master, hcloud_server.worker]
}