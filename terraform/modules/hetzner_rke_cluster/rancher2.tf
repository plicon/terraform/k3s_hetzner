resource "rancher2_cluster" "rke_cluster" {
  name                                    = var.cluster
  default_pod_security_policy_template_id = "restricted"

  enable_cluster_alerting                 = false

  enable_cluster_monitoring               = false
  enable_network_policy                   = var.network_policy
  rke_config {
    kubernetes_version      = "v1.20.8-rancher1-1"
    cloud_provider {
      name                  = "external"
    }
    addons_include          = [
      "https://raw.githubusercontent.com/hetznercloud/hcloud-cloud-controller-manager/master/deploy/ccm-networks.yaml"
    ]
    ingress {
      provider              = "none"
    }
    network {
      canal_network_provider {
        iface               = var.cni_interface
      }
      plugin                = var.cni_plugin
    }
    services {
      etcd {
        backup_config {
          enabled           = true
          interval_hours    = "12"
          retention         = "6"
          safe_timestamp    = false
        }
        creation            = "12h"
      }
      kube_api {
        always_pull_images  = true
      }
      kube_controller {
        cluster_cidr        = "10.244.0.0/16"
      }
      kubelet {
        extra_args          = {
          cloud-provider    = "external"
        }
      }
    }
  }
  depends_on = [null_resource.ansible]
}

resource "rancher2_token" "terraform" {
  cluster_id  = rancher2_cluster.rke_cluster.id
  description = "Terraform access token"
  ttl         = 604800
}

# Register nodes in cluster
resource "null_resource" "register_master_nodes" {
  count = var.num_master
  connection {
    type        = "ssh"
    user        = var.ansible_user
    private_key = file(var.ssh_key_private_path)
    host        = hcloud_server.master[count.index].ipv4_address
  }

  provisioner "remote-exec" {
    inline = [
      format("%s --address %s --internal-address %s --node-name %s %s --label %s",
        rancher2_cluster.rke_cluster.cluster_registration_token[0]["node_command"],
        hcloud_server.master[count.index].ipv4_address,
        hcloud_server_network.master_network[count.index].ip,
        hcloud_server.master[count.index].name,
        "--etcd --controlplane",
        "app=wireguard"
      )
    ]
  }
  depends_on = [rancher2_cluster.rke_cluster]
}

resource "null_resource" "register_worker_nodes" {
  count = var.num_worker
  connection {
    type        = "ssh"
    user        = var.ansible_user
    private_key = file(var.ssh_key_private_path)
    host        = hcloud_server.worker[count.index].ipv4_address
  }

  provisioner "remote-exec" {
    inline = [
      format("%s --address %s --internal-address %s --node-name %s %s",
        rancher2_cluster.rke_cluster.cluster_registration_token[0]["node_command"],
        hcloud_server.worker[count.index].ipv4_address,
        hcloud_server_network.worker_network[count.index].ip,
        hcloud_server.worker[count.index].name,
        "--worker"
      )
    ]
  }
  depends_on = [rancher2_cluster.rke_cluster]
}

resource "rancher2_cluster_sync" cluster {
  cluster_id      = rancher2_cluster.rke_cluster.id
  wait_monitoring = false
  state_confirm   = 12
  depends_on      = [null_resource.register_master_nodes, null_resource.register_worker_nodes]
}

resource "local_file" "kubeconfig" {
  content     = rancher2_cluster.rke_cluster.kube_config
  filename    = pathexpand(var.kube_config)
}

# Hcloud secret required for hcloud cloudcontroller
resource "rancher2_secret" "hcloud-secret" {
  name          = "hcloud"
  description   = "HCLOUD  Secret"
  project_id    = rancher2_cluster.rke_cluster.system_project_id
  data          = {
    token   = base64encode(var.hcloud_token)
    network = base64encode(var.network)
  }
}