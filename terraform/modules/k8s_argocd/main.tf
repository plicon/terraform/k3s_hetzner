resource "rancher2_namespace" "argocd" {
  name          = "argocd"
  project_id    = var.project_id
}

## Argo expects the password in the secret to be bcrypt hashed. You can create this hash with
## `htpasswd -nbBC 10 "" $ARGO_PWD | tr -d ':\n' | sed 's/$2y/$2a/'`
resource "rancher2_secret" "argocd_gitlab_credentials" {
  name          = "argocd-gitlab-credentials"
  description   = "gitlab credentials for ArgoCD"
  project_id    = var.project_id
  namespace_id  = rancher2_namespace.argocd.id
  data          = {
    gitlabPrivateKey = base64encode(file(pathexpand(var.gitlab_privatekey)))
  }
}

resource "rancher2_app_v2" "argocd" {
  cluster_id    = var.cluster_id
  project_id    = var.project_id
  name          = "argo"
  namespace     = rancher2_namespace.argocd.id
  repo_name     = "argo"
  chart_name    = "argo-cd"
  chart_version = var.chart_version
  wait          = true
  values        = templatefile(
                      "${path.module}/values/values.tpl",
                      {
                        hostname = "${var.argo_subdomain}.${var.argo_domain}"
                        argoServerAdminPassword = var.argo_admin_bcrypted
                        target_revision = var.argo_target_revision
                        secret_name = "argocd-gitlab-credentials"
                        secret_key = "gitlabPrivateKey"
                      }
                  )
}

########################
# CONfigure CloudFlare #
########################
resource "cloudflare_record" "argocd" {
  zone_id = var.cloudflare_zone_id
  name    = "${var.argo_subdomain}.${var.argo_domain}"
  value   = "${var.cf_value}.${var.argo_domain}"
  type    = "CNAME"
  proxied = true
}
# Create acess application and policy
resource "cloudflare_access_application" "argocd-access" {
  zone_id                   = var.cloudflare_zone_id
  name                      = "ArgoCD Application"
  domain                    = "${var.argo_subdomain}.${var.argo_domain}"
  type                      = "self_hosted"
  session_duration          = "8h"
  auto_redirect_to_identity = false
}

# Allowing access to `@plicon.nl` email address only
resource "cloudflare_access_policy" "argocd-access" {
  application_id = cloudflare_access_application.argocd-access.id
  zone_id        = var.cloudflare_zone_id
  name           = "ArgoCD policy"
  precedence     = "1"
  decision       = "allow"

  include {
    email_domain = [ var.argo_domain ]
  }

  require {
    email_domain = [ var.argo_domain ]
  }
}