## ArgoCD configurationas
## Ref: https://github.com/argoproj/argo-cd
##
nameOverride: argocd

# Optional CRD installation for those without Helm hooks
installCRDs: false

global:
  securityContext:
    runAsUser: 999
    runAsGroup: 999
    fsGroup: 999
  imagePullSecrets: []
  hostAliases: []
  # - ip: 10.20.30.40
  #   hostnames:
  #   - git.myhostname

## Controller
controller:
  ## Readiness and liveness probes for default backend
  ## Ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/
  ##
  readinessProbe:
    failureThreshold: 3
    initialDelaySeconds: 10
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1
  livenessProbe:
    failureThreshold: 3
    initialDelaySeconds: 10
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1

  resources:
    limits:
      cpu: 500m
      memory: 512Mi
    requests:
      cpu: 250m
      memory: 256Mi

  ## Server metrics controller configuration
  metrics:
    enabled: false
    service:
      annotations:
        prometheus.io/port: "8082"
        prometheus.io/scrape: "true"
      labels: {}
      servicePort: 8082
    serviceMonitor:
      enabled: false
    #   selector:
    #     prometheus: kube-prometheus
    #   namespace: monitoring
    #   additionalLabels: {}

  ## Enable Admin ClusterRole resources.
  ## Enable if you would like to grant rights to ArgoCD to deploy to the local Kubernetes cluster.
  clusterAdminAccess:
    enabled: true

## Dex
dex:
  enabled: false

## Redis
redis:
  enabled: true
  name: redis

  image:
    repository: redis
    tag: 5.0.8
    imagePullPolicy: IfNotPresent

  resources:
    limits:
      cpu: 200m
      memory: 128Mi
    requests:
      cpu: 100m
      memory: 64Mi

  volumeMounts: []
  volumes: []

redis-ha:
  enabled: false

## Server
server:
  name: server

  replicas: 2

  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 5
    targetCPUUtilizationPercentage: 50
    targetMemoryUtilizationPercentage: 50

  extraArgs:
    - --insecure

  ## Argo server log format: text|json
  logFormat: text
  ## Argo server log level
  logLevel: info

  ## Readiness and liveness probes for default backend
  ## Ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/
  ##
  readinessProbe:
    failureThreshold: 3
    initialDelaySeconds: 10
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1
  livenessProbe:
    failureThreshold: 3
    initialDelaySeconds: 10
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1

  resources:
    limits:
      cpu: 100m
      memory: 128Mi
    requests:
      cpu: 50m
      memory: 64Mi

  ## Server metrics service configuration
  metrics:
    enabled: false
    service:
      annotations:
        prometheus.io/port: "8083"
        prometheus.io/scrape: "true"
      labels: {}
      servicePort: 8083
    serviceMonitor:
      enabled: false
    #   selector:
    #     prometheus: kube-prometheus
    #   namespace: monitoring
    #   additionalLabels: {}

  ingress:
    enabled: true
    annotations: {}
    labels: {}
    ## Argo Ingress.
    ## Hostnames must be provided if Ingress is enabled.
    ## Secrets must be manually created in the namespace
    ##
    hosts:
      - ${hostname}
    paths:
      - /
    tls:
      []
      # - secretName: argocd-example-tls
      #   hosts:
      #     - argocd.example.com
    https: false

  ## ArgoCD config
  ## reference https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/argocd-cm.yaml
  config:
    # Argo CD's externally facing base URL (optional). Required when configuring SSO
    #url: http://argocd-dev.plicon.nl
    # Argo CD instance label key
    application.instanceLabelKey: argocd.argoproj.io/instance
    repositories: |
      - url: git@gitlab.com:argocd/app-of-apps.git
        type: git
        sshPrivateKeySecret:
          name: ${secret_name}
          key: ${secret_key}
      - url: git@gitlab.com:argocd/app_hue-api-gateway.git
        type: git
        sshPrivateKeySecret:
          name: ${secret_name}
          key: ${secret_key}
#      - url: git@gitlab.com:argocd/mattermost.git
#        type: git
#        sshPrivateKeySecret:
#          name: ${secret_name}
#          key: ${secret_key}
  ## ArgoCD rbac config
  ## reference https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/rbac.md

  ## Projects
  ## reference: https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/
  additionalProjects:
    - name: default
      namespace: argocd
      description: Default Project
      sourceRepos:
        - '*'
      destinations:
        - namespace: '*'
          server: '*'
      clusterResourceWhitelist:
        - group: '*'
          kind: '*'
  ## Applications
  ## reference: https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/
  additionalApplications:
    - name: "app-off-apps"
      namespace: "argocd"
      project: "default"
      source:
        path: .
        repoURL: 'git@gitlab.com:argocd/app-of-apps.git'
        targetRevision: ${target_revision}
        helm:
          parameters:
            - name: spec.source.targetRevision
              value: ${target_revision}
          valueFiles:
            - values.yaml
      destination:
        namespace: "argocd"
        server: "https://kubernetes.default.svc"
      syncPolicy:
        automated:
          prune: true
          selfHeal: true

  ## Enable Admin ClusterRole resources.
  ## Enable if you would like to grant rights to ArgoCD to deploy to the local Kubernetes cluster.
  clusterAdminAccess:
    enabled: true

  ## Enable BackendConfig custom resource for Google Kubernetes Engine


## Repo Server
repoServer:
  name: repo-server

  replicas: 1
  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 5
    targetCPUUtilizationPercentage: 50
    targetMemoryUtilizationPercentage: 50

  ## Readiness and liveness probes for default backend
  ## Ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/
  ##
  readinessProbe:
    failureThreshold: 3
    initialDelaySeconds: 10
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1
  livenessProbe:
    failureThreshold: 3
    initialDelaySeconds: 10
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1

  ## Additional volumeMounts to the repo server main container.
  volumeMounts: []
  ## Additional volumes to the repo server pod.
  volumes: []

  resources:
    limits:
      cpu: 50m
      memory: 128Mi
    requests:
      cpu: 10m
      memory: 64Mi

  ## Repo server metrics service configuration
  metrics:
    enabled: false
    service:
      annotations:
        prometheus.io/port: "8084"
        prometheus.io/scrape: "true"
      labels: {}
      servicePort: 8084
    serviceMonitor:
      enabled: false
    #   selector:
    #     prometheus: kube-prometheus
    #   namespace: monitoring
    #   additionalLabels: {}

## Argo Configs
configs:
  secret:
    createSecret: true
    ## Annotations to be added to argocd-secret
    ##
    # Argo expects the password in the secret to be bcrypt hashed. You can create this hash with
    # `htpasswd -nbBC 10 "" $ARGO_PWD | tr -d ':\n' | sed 's/$2y/$2a/'`
    argocdServerAdminPassword: ${argoServerAdminPassword}
    # Password modification time defaults to current time if not set
    # argocdServerAdminPasswordMtime: "2020-12-10T10:00:00Z"

openshift:
  enabled: false