# Set the variable value in *.tfvars file
#------------------------------------------------------
# GENERAL
#------------------------------------------------------

variable "cluster_id" {}
variable "project_id" {}

variable "cloudflare_zone_id" {}
variable "argo_admin_bcrypted" {}
variable "argo_subdomain" {}
variable "argo_domain" {}
variable "argo_target_revision" {}
variable "cf_value" {}

variable "gitlab_privatekey" {}

variable "chart_version" {
  default = "3.7.1"
}