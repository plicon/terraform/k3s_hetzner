# Set the variable value in *.tfvars file
#------------------------------------------------------
# GENERAL
#------------------------------------------------------
variable "domain" {
  default = "plicon.nl"
}
variable "wp_appname" {
  default = "wordpress"
}
variable "wp_namespace" {
  default   = "wordpress"
}
variable "wp_tpl_version" {
  default  = "10.0.2"
}
variable "cluster_id" {}
variable "project_id" {}
variable "cloudflare_zone_id" {}
variable "cf_name" {}
variable "cf_value" {}