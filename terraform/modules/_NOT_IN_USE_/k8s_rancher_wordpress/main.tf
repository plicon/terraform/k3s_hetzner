resource "rancher2_namespace" "wordpress" {
  name        = var.wp_namespace
  project_id  = var.project_id
}

# Install wordpress also includes mariadb
resource "rancher2_app_v2" "wp-dev" {
  cluster_id    = var.cluster_id
  project_id    = var.project_id
  name          = var.wp_appname
  namespace     = rancher2_namespace.wordpress.id
  repo_name     = "bitnami"
  chart_name    = "wordpress"
  chart_version = var.wp_tpl_version
  values        = file("${path.module}/values/wp-dev_values.yaml")

}

## Create DNS records for wirdoress site @ CloudFlare
resource "cloudflare_record" "wordpress" {
  zone_id = var.cloudflare_zone_id
  name    = "${var.cf_name}.${var.domain}"
  value   = "${var.cf_value}.${var.domain}"
  type    = "CNAME"
  proxied = true
}