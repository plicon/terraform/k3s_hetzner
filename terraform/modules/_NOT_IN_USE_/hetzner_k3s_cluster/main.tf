#####################################
# Create master node(s)
resource "hcloud_server" "master" {
  count       = var.num_master
  name        = format("k3s-master%02d", count.index +1)
  image       = var.image_name
  server_type = var.master_type

  labels = merge(
    var.custom_tags,
    map (
      "Role", "master",
      "Name", format("k3s-master%02d", count.index +1)
    )
  )

  user_data = <<-EOF
    #cloud-config
    manage_etc_hosts: true
    preserve_hostname: false
    hostname: "${format("k3s-master%02d", count.index +1)}"
    fqdn: "${format("k3s-master%02d", count.index +1)}.${var.subdomain}.${var.domain}"
    users:
      - name: "${var.ansible_user}"
        shell: /bin/bash
        sudo: ALL=(ALL) NOPASSWD:ALL
        ssh_authorized_keys:
          - "${file(var.ssh_key_public_path)}"
  EOF
}
# Create worker node(s)
resource "hcloud_server" "worker" {
  count       = var.num_worker
  name        = format("k3s-worker%02d", count.index +1)
  image       = var.image_name
  server_type = var.worker_type

  labels = merge(
    var.custom_tags,
    map (
      "Role", "worker",
      "Name", format("k3s-worker%02d", count.index +1)
    )
  )

  user_data = <<-EOF
    #cloud-config
    manage_etc_hosts: true
    preserve_hostname: false
    hostname: "${format("k3s-worker%02d", count.index +1)}"
    fqdn: "${format("k3s-worker%02d", count.index +1)}.${var.subdomain}.${var.domain}"
    users:
      - name: "${var.ansible_user}"
        shell: /bin/bash
        sudo: ALL=(ALL) NOPASSWD:ALL
        ssh_authorized_keys:
          - "${file(var.ssh_key_public_path)}"
  EOF
}
####################################
# Create network
resource "hcloud_network" "network" {
  name      = var.network
  ip_range  = var.ip_range
  labels    = var.custom_tags
}
# Add subnet to network
resource "hcloud_network_subnet" "subnet" {
  network_id    = hcloud_network.network.id
  type          = "server"
  network_zone  = var.network_zone
  ip_range      = var.ip_subnet
}
# Bind network to nodes
resource "hcloud_server_network" "master_network" {
  count       = var.num_master
  server_id   = hcloud_server.master[count.index].id
  subnet_id   = hcloud_network_subnet.subnet.id
}
resource "hcloud_server_network" "worker_network" {
  count       = var.num_worker
  server_id   = hcloud_server.worker[count.index].id
  subnet_id   = hcloud_network_subnet.subnet.id
}

# Create Load balancer
resource "hcloud_load_balancer" "lb" {
  name                = "k3s_lb"
  load_balancer_type  = "lb11"
  location            = var.location
}
resource "hcloud_load_balancer_network" "lb_network" {
  load_balancer_id    = hcloud_load_balancer.lb.id
  subnet_id           = hcloud_network_subnet.subnet.id
}
resource "hcloud_load_balancer_target" "k3s_lb_target" {
  load_balancer_id    = hcloud_load_balancer.lb.id
  type                = "label_selector"
  label_selector      = "Application=k3s"
  use_private_ip      = true
  depends_on          = [
    hcloud_server_network.master_network,
    hcloud_server_network.worker_network,
  ]
}
resource "hcloud_load_balancer_service" "lb_service_80" {
    load_balancer_id  = hcloud_load_balancer.lb.id
    protocol          = "tcp"
    listen_port       = 80
    destination_port  = 80
}
resource "hcloud_load_balancer_service" "lb_service_443" {
    load_balancer_id  = hcloud_load_balancer.lb.id
    protocol          = "tcp"
    listen_port       = 443
    destination_port  = 443
}
