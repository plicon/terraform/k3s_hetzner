
# Create DNS records for master node(s) @ CloudFlare
resource "cloudflare_record" "master" {
  count   = var.num_master
  zone_id = var.cloudflare_zone_id
  name    = "${hcloud_server.master[count.index].name}.${var.subdomain}"
  value   = hcloud_server.master[count.index].ipv4_address
  type    = "A"
  ttl     = 60
}
# Create DNS records for worker node(s) @ CloudFlare
resource "cloudflare_record" "worker" {
  count   = var.num_worker
  zone_id = var.cloudflare_zone_id
  name    = "${hcloud_server.worker[count.index].name}.${var.subdomain}"
  value   = hcloud_server.worker[count.index].ipv4_address
  type    = "A"
  ttl     = 60
}

resource "cloudflare_record" "lb_wildcard_ipv4" {
  zone_id = var.cloudflare_zone_id
  name    = "*.${var.subdomain}"
  value   = hcloud_load_balancer.lb.ipv4
  type    = "A"
  ttl     = 1
}

resource "cloudflare_record" "lb_wildcard_ipv6" {
  zone_id = var.cloudflare_zone_id
  name    = "*.${var.subdomain}"
  value   = hcloud_load_balancer.lb.ipv6
  type    = "AAAA"
  ttl     = 1
}