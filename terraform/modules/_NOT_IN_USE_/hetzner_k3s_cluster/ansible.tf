###############################################
# Run ansible-playbook on the created machines
locals {
  root_dir            = "../../../"
  ansible_dir         = "./ansible/"
  ansible_extra_vars = var.ansible_extra_vars != "" ? "--extra-vars '${var.ansible_extra_vars}'" : ""
}
resource "null_resource" "ansible" {
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = <<EOT
      rm -rf ansible
      git clone git@gitlab.com:plicon/ansible-playbooks/hetzner_k3s.git ansible
    EOT
  }

  provisioner "local-exec" {
    command = <<EOT
      ansible-galaxy install -r roles/requirements.yaml -p roles --force
      echo "ansible-galaxy collection install -r collections/requirements.yaml -p collections --force"
    EOT
    working_dir = local.ansible_dir
  }

  provisioner "local-exec" {
    environment = {
        HCLOUD_TOKEN = var.hcloud_token
    }
    command = <<EOT
        ansible-playbook ./${var.ansible_playbook} -i inventory_hcloud.yaml -v ${var.ansible_extra_opts} ${local.ansible_extra_vars}
    EOT
    working_dir = local.ansible_dir
  }
  depends_on = [hcloud_server.master, hcloud_server.worker]
}

resource "null_resource" "ansible_cleanup" {
  provisioner "local-exec" {
    when = destroy
    command = <<EOT
      rm -rf ./ansible
    EOT
  }
}