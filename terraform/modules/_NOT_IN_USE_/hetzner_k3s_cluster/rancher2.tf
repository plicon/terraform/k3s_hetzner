# Create cluster in Rancher
resource "rancher2_cluster" "k3s_imported" {
  name = var.cluster
  description = var.cluster

  enable_cluster_monitoring = true
  depends_on = [null_resource.ansible]
}

resource "null_resource" "register_cluster" {
  provisioner "local-exec" {
    environment = {
      KUBECONFIG = pathexpand(var.kube_config)
    }
    command = <<EOT
      kubectl apply -f ${rancher2_cluster.k3s_imported.cluster_registration_token[0].manifest_url}
      kubectl -n cattle-system wait --for=condition=available --timeout=300s deployment/cattle-cluster-agent
      sleep 30
    EOT
  }
}
resource "rancher2_project" "infra" {
  name                      = "infra"
  cluster_id                = rancher2_cluster.k3s_imported.id
  enable_project_monitoring = true
  depends_on                = [null_resource.register_cluster]
}
####### LONGHORN
resource "rancher2_namespace" "longhorn-system" {
  name = "longhorn-system"
  project_id    = rancher2_project.infra.id
}

resource "rancher2_app" "longhorn" {
  catalog_name      = "library"
  name              = "longhorn-system"
  project_id        = rancher2_project.infra.id
  target_namespace  = rancher2_namespace.longhorn-system.id
  template_name     = "longhorn"
  template_version  = "1.0.2"
  answers = {
    "csi.kubeletRootDir" = "/var/lib/kubelet"
    "defaultSettings.autoSalvage" = "true"
    "defaultSettings.backupTarget" = "s3://longhorn-s3-backup@eu-central-1/"
    "defaultSettings.backupTargetCredentialSecret" = "s3-secret"
    "defaultSettings.backupstorePollInterval" = "300"
    "defaultSettings.createDefaultDiskLabeledNodes" = "false"
    "defaultSettings.defaultDataPath" = "/var/lib/longhorn/"
    "defaultSettings.defaultLonghornStaticStorageClass" = "longhorn-static"
    "defaultSettings.defaultReplicaCount" = "2"
    "defaultSettings.disableSchedulingOnCordonedNode" = "true"
    "defaultSettings.guaranteedEngineCPU" = "0.25"
    "defaultSettings.replicaSoftAntiAffinity" = "false"
    "defaultSettings.replicaZoneSoftAntiAffinity" = "true"
    "defaultSettings.storageMinimalAvailablePercentage" = "25"
    "defaultSettings.storageOverProvisioningPercentage" = "200"
    "defaultSettings.upgradeChecker" = "true"
    "defaultSettings.volumeAttachmentRecoveryPolicy" = "wait"
    "enablePSP" = "true"
    "image.defaultImage" = "true"
    "ingress.enabled" = "false"
    "longhorn.default_setting" = "true"
    "persistence.defaultClass" = "true"
    "persistence.defaultClassReplicaCount" = "2"
    "service.ui.type" = "Rancher-Proxy"
  }
}

resource "rancher2_secret" "s3-secret" {
  name          = "s3-secret"
  description   = "S3 Longhong backup secret"
  project_id    = rancher2_project.infra.id
  namespace_id  = rancher2_namespace.longhorn-system.id
  data          = {
    AWS_ACCESS_KEY_ID = base64encode("AKIAWFXHLHAJI7C53C4U")
    AWS_SECRET_ACCESS_KEY = base64encode("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")
  }
}
########### MARIADB
resource "rancher2_project" "apps" {
  name                      = "apps"
  cluster_id                = rancher2_cluster.k3s_imported.id
  enable_project_monitoring = false
  depends_on                = [null_resource.register_cluster]
}
resource "rancher2_namespace" "mariadb" {
  name = "mariadb"
  project_id    = rancher2_project.apps.id
}

resource "rancher2_app" "mariadb" {
  catalog_name      = "library"
  name              = "mariadb"
  project_id        = rancher2_project.apps.id
  target_namespace  = rancher2_namespace.mariadb.id
  template_name     = "mariadb"
  template_version  = "7.3.14"
  answers = {
    "db.name" = "wordpressdb1"
    "db.password" = "wordpressdb1"
    "db.user" = "wordpressdb1"
    "defaultImage" = "true"
    "master.persistence.enabled" = "true"
    "master.persistence.size" = "8Gi"
    "master.persistence.storageClass" = "longhorn"
    "metrics.enabled" = "true"
    "replication.enabled" = "true"
    "service.type" = "ClusterIP"
    "slave.persistence.enabled" = "false"
    "slave.replicas" = "2"
  }
  depends_on = [rancher2_app.longhorn]
}