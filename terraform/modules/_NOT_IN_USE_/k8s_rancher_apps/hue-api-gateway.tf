## Install and configure HUE-API-GATEWAY
#resource "rancher2_namespace" "iot" {
#  name        = "iot"
#  project_id  = rancher2_project.hosting.id
#}
#
#resource "kubernetes_config_map" "hue-api-gateway-config" {
#  metadata {
#    name = "hue-api-gateway-config"
#    labels = {
#      app = "hue-api-gateway"
#    }
#    namespace = "iot"
#  }
#
#  data = {
#    PORT = "80"
#    HUE_URL = "http://philips-hue.home.knieptang.nl"
#    HUE_TOKEN = "yhbKSsESjtYHfvh63dDvc-OTig4mf-LJ3wep2CiM"
#    HUE_SCENE_THEATER = "eeTqpqbPp6o1oSR"
#    HUE_SCENE_DIMMED = "AjPg3uZpN2e15a7"
#    HUE_GROUP_ID = "1"
#    PLEX_PLAYER_UUID = "8653be2aa4edbcbd-com-plexapp-android"
#  }
#}
#
#resource "kubernetes_deployment" "hue-api-gateway" {
#  depends_on        = [rancher2_app.ingress-nginx]
#  metadata {
#    name =  "hue-api-gateway"
#    labels = {
#      run = "hue-api-gateway"
#    }
#    namespace = "iot"
#  }
#
#  spec {
#    replicas = 1
#
#    selector {
#      match_labels = {
#        run = "hue-api-gateway"
#      }
#    }
#
#    template {
#      metadata {
#        labels = {
#          run = "hue-api-gateway"
#          app = "nodejs"
#        }
#      }
#      spec {
#        node_selector = {
#          app = "wireguard"
#        }
#        dns_policy = "None"
#        dns_config {
#          nameservers = [
#            "192.168.1.1",
#            "1.1.1.1",
#          ]
#          searches = [
#            "home.knieptang.nl",
#            "iot.knieptang.nl",
#          ]
#        }
#        container {
#          image = "registry.gitlab.com/knieptang/hue_api_gateway:1.4"
#          name = "hue-api-gateway"
#
#          env_from {
#            config_map_ref {
#              name = "hue-api-gateway-config"
#            }
#          }
#        }
#      }
#    }
#  }
#}
#
#resource "kubernetes_service" "hue-api-srv" {
#  depends_on        = [rancher2_app.ingress-nginx]
#  metadata {
#    name = "hue-api-srv"
#    labels = {
#      app = "hue-api-gateway"
#    }
#    namespace = "iot"
#  }
#  spec {
#    selector = {
#      run = "hue-api-gateway"
#    }
#    port {
#      name = "hue-api-port"
#      port = 80
#      protocol = "TCP"
#      target_port = 80
#    }
#    type = "ClusterIP"
#  }
#}
#
#resource "kubernetes_ingress" "hue-api-gateway" {
#  depends_on        = [rancher2_app.ingress-nginx]
#  metadata {
#    name = "hue-api-gateway"
#    labels = {
#      app = "hue-api-gateway"
#    }
#    namespace = "iot"
#  }
#
#  spec {
#    rule {
#      host = "hue-api-v2.plicon.nl"
#      http {
#        path {
#          backend {
#            service_name = "hue-api-srv"
#            service_port = "hue-api-port"
#          }
#        }
#      }
#    }
#  }
#}