resource "rancher2_namespace" "wp-dev" {
  name = "wpdev"
  project_id    = rancher2_project.hosting.id
  depends_on    = [rancher2_catalog.bitnami]
}

# Install wordpress also includes mariadb
resource "rancher2_app" "wp-dev" {
  catalog_name      = "bitnami"
  name              = "wp-plicon-dev"
  project_id        = rancher2_project.hosting.id
  target_namespace  = rancher2_namespace.wp-dev.id
  template_name     = "wordpress"
  template_version  = "10.0.2"
  values_yaml       = base64encode(file("${path.module}/values/wp-dev_values.yaml"))
}

## Create DNS records for wirdoress site @ CloudFlare
resource "cloudflare_record" "wp-dev" {
  zone_id = var.cloudflare_zone_id
  name    = "www-dev-hcl.plicon.nl"
  value   = "argo-k8s-hcl.plicon.nl."
  type    = "CNAME"
  proxied = true
}