#---------------------------------------------------------------------
# RANCHER APP INSTALLATION/CONFIGURATION
#---------------------------------------------------------------------
# Create hosting project
resource "rancher2_project" "hosting" {
  name                      = "Hosting"
  cluster_id                = var.cluster_id
}

# Add Catalog
resource "rancher2_catalog" "bitnami" {
  name    = "bitnami"
  url     = "https://charts.bitnami.com/bitnami"
  branch  = "master"
  scope   = "global"
  refresh = "true"
  version = "helm_v3"
}
resource "rancher2_catalog" "ingress-nginx" {
  name    = "ingress-nginx"
  url     = "https://kubernetes.github.io/ingress-nginx"
  branch  = "master"
  scope   = "global"
  refresh = "true"
  version = "helm_v3"
}
resource "rancher2_catalog" "jetstack" {
  name    = "jetstack"
  url     = "https://charts.jetstack.io"
  branch  = "master"
  scope   = "global"
  refresh = "true"
  version = "helm_v3"
}