resource "rancher2_namespace" "external-dns" {
  name = "external-dns"
  project_id    = rancher2_project.hosting.id
  depends_on    = [rancher2_catalog.bitnami]
}
# Install external-dns
resource "rancher2_app" "external-dns" {
  catalog_name      = "bitnami"
  name              = "external-dns"
  project_id        = rancher2_project.hosting.id
  target_namespace  = rancher2_namespace.external-dns.id
  template_name     = "external-dns"
  template_version  = "4.0.0"
  values_yaml       = base64encode(file("${path.module}/values/external-dns_values.yaml"))
}