# Set the variable value in *.tfvars file
#------------------------------------------------------
# GENERAL
#------------------------------------------------------
variable "cluster_id" {
  default = ""
}
variable "domain" {
  default = "plicon.nl"
}
variable "kube_config" {
  default = "~/.kube/config"
}
variable "cloudflare_zone_id" {
  default = "0"
}
variable "cloudflare_argo_cert_key_file" {
  default = "~/.cloudflared/cert.pem"
}
variable "rancher_version" {
  default = "stable"
}
variable "rancher_url" {
  default = "https://rancher.plicon.nl"
}
variable "rancher_admin_password" {
  default   = "VerySup4hSecretzz"
}