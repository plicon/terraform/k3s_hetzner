#--------------------------------------------------------
# Setup cluster for NGINX INGRESS with CLOUDFLARE ARGO Sidecar container
#
resource "rancher2_namespace" "ingress-nginx" {
  name = "ingress-nginx"
  project_id    = rancher2_project.hosting.id
}

resource "rancher2_secret" "cf-argo-tunnel-secret" {
  name          = "cf-argo-tunnel-secret"
  description   = "CloudFlare ARGO Tunnel Secret"
  project_id    = rancher2_project.hosting.id
  namespace_id  = rancher2_namespace.ingress-nginx.id
  data          = {
    "cert.pem"  = base64encode(file(pathexpand(var.cloudflare_argo_cert_key_file)))
  }
}

# Install Ingress Nginx with ARG-Sidecar Container
resource "rancher2_app" "ingress-nginx" {
  catalog_name      = "ingress-nginx"
  name              = "ingress-nginx"
  project_id        = rancher2_project.hosting.id
  target_namespace  = rancher2_namespace.ingress-nginx.id
  template_name     = "ingress-nginx"
  template_version  = "3.11.0"
  values_yaml       = base64encode(file("${path.module}/values/ingress-nginx_values.yaml"))
  depends_on        = [rancher2_catalog.ingress-nginx]
}