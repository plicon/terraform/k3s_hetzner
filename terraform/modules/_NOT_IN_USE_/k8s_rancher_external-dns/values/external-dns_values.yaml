---
## Bitnami external-dns image version
## ref: https://hub.docker.com/r/bitnami/external-dns/tags/
##
image:
  registry: docker.io
  repository: bitnami/external-dns
  tag: 0.7.4-debian-10-r41
  pullPolicy: IfNotPresent

## K8s resources type to be observed for new DNS entries by ExternalDNS
##
sources:
  # - crd
  - service
  # - contour-httpproxy

## DNS provider where the DNS records will be created. Available providers are:
##
provider: cloudflare

## Flags related to processing sources
## ref: https://github.com/kubernetes-sigs/external-dns/blob/master/pkg/apis/externaldns/types.go#L272
##
## Limit sources of endpoints to a specific namespace (default: all namespaces)
##
namespace: ""
## Templated strings that are used to generate DNS names from sources that don't define a hostname themselves
##
fqdnTemplates: []
## Combine FQDN template and annotations instead of overwriting
##
combineFQDNAnnotation: false
## Ignore hostname annotation when generating DNS names, valid only when fqdn-template is set
##
ignoreHostnameAnnotation: false
## Allow external-dns to publish DNS records for ClusterIP services
##
publishInternalServices: false
## Allow external-dns to publish host-ip for headless services
##
publishHostIP: false
## The service types to take care about (default: all, options: ClusterIP, NodePort, LoadBalancer, ExternalName)
##
serviceTypeFilter: []

## Alibaba cloud configuration to be set via arguments/env. variables
##
## Cloudflare configuration to be set via arguments/env. variables
##
cloudflare:
  ## `CF_API_TOKEN` to set in the environment
  ##
  apiToken: "Hxg5-mlGcEb-ev3dVc6-GuvYg2gJZT_5bgw33yuD"
  apiKey: ""
  email: ""
  ## Enable the proxy feature of Cloudflare
  ##
  proxied: false


## OpenStack De
## Limit possible target zones by domain suffixes (optional)
##
domainFilters:
  - hcl.plicon.nl
## Exclude subdomains (optional)
##
excludeDomains: []
## Limit possible target zones by zone id (optional)
##
zoneIdFilters:
  - "80ee883786b61469886684c7cbc66fd4"
## Filter sources managed by external-dns via annotation using label selector semantics (optional)
##
annotationFilter: ""
## When enabled, prints DNS record changes rather than actually performing them
##
dryRun: false
## When enabled, triggers run loop on create/update/delete events (optional, in addition of regular interval)
##
triggerLoopOnEvent: false
## Adjust the interval for DNS updates
##
interval: "1m"
## Verbosity of the ExternalDNS logs. Available values are:
## - panic, debug, info, warn, error, fatal
##
logLevel: debug
## Formats of the ExternalDNS logs. Available values are:
## - text, json
##
logFormat: text
## Modify how DNS records are sychronized between sources and providers (options: sync, upsert-only)
##
policy: upsert-only
## Registry Type. Available types are: txt, noop
## ref: https://github.com/kubernetes-sigs/external-dns/blob/master/docs/proposal/registry.md
##
registry: "txt"
## TXT Registry Identifier
##
txtOwnerId: ""
## Prefix to create a TXT record with a name following the pattern prefix.<CNAME record>
##
# txtPrefix: ""

## Extra Arguments to passed to external-dns
##
extraArgs: {}
## Extra env. variable to set on external-dns container.
##
## extraEnv:
## - name: VARNAME1
##   value: value1
## - name: VARNAME2
##   valueFrom:
##     secretKeyRef:
##       name: existing-secret
##       key: varname2-key
extraEnv: []

## Replica count
##
replicas: 1

## Affinity for pod assignment (this value is evaluated as a template)
## Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
##
affinity: {}
## Node labels for pod assignment (this value is evaluated as a template)
## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#nodeselector
##
nodeSelector: {}
## Tolerations for pod assignment (this value is evaluated as a template)
## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#taints-and-tolerations-beta-feature
##
tolerations: []
## Annotations for external-dns pods
##
podAnnotations: {}
## Additional labels for the pod(s).
##
podLabels: {}
## Pod priority class name
##
priorityClassName: ""

## Options for the source type "crd"
##
crd:
  ## Install and use the integrated DNSEndpoint CRD
  create: false
  ## Change these to use an external DNSEndpoint CRD (E.g. from kubefed)
  apiversion: ""
  kind: ""

## Kubernetes svc configutarion
##
service:
  ## Kubernetes svc type
  ##
  type: ClusterIP
  port: 7979
  ## Specify the nodePort value for the LoadBalancer and NodePort service types for the client port
  ## ref: https://kubernetes.io/docs/concepts/services-networking/service/#type-nodeport
  ##
  # nodePort:
  ## Static clusterIP or None for headless services
  ##
  # clusterIP: ""
  ## External IP list to use with ClusterIP service type
  ##
  externalIPs: []
  ## Use loadBalancerIP to request a specific static IP,
  ## otherwise leave blank
  ##
  # loadBalancerIP:
  ## Address that are allowed when svc is LoadBalancer
  ##
  loadBalancerSourceRanges: []
  ## Provide any additional annotations which may be required. This can be used to
  ## set the LoadBalancer service type to internal only.
  ## ref: https://kubernetes.io/docs/concepts/services-networking/service/#internal-load-balancer
  ##
  annotations: {}

  ## Provide any additional labels which may be required. This can be used to
  ## have external-dns show up in `kubectl cluster-info`
  ##  kubernetes.io/cluster-service: "true"
  ##  kubernetes.io/name: "external-dns"
  labels: {}

## ServiceAccount parameters
## https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
##
serviceAccount:
  create: true
  ## Service Account for pods
  ##
  name:
  ## Annotations for the Service Account
  ##
  annotations: {}

## RBAC parameteres
## https://kubernetes.io/docs/reference/access-authn-authz/rbac/
##
rbac:
  create: true
  ## Deploys ClusterRole by Default
  clusterRole: true
  ## RBAC API version
  ##
  apiVersion: v1beta1
  ## Podsecuritypolicy
  ##
  pspEnabled: false

## Kubernetes Security Context
## https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
## Example:
## securityContext:
##   allowPrivilegeEscalation: false
##   readOnlyRootFilesystem: true
##   capabilities:
##     drop: ["ALL"]
##
securityContext: {}
podSecurityContext:
  fsGroup: 1001
  runAsUser: 1001
  # runAsNonRoot: true

## Configure resource requests and limits
## ref: http://kubernetes.io/docs/user-guide/compute-resources/
##
resources: {}
#  limits:
#    cpu: 50m
#    memory: 50Mi
#  requests:
#    memory: 50Mi
#    cpu: 10m

## Liveness Probe. The block is directly forwarded into the deployment, so you can use whatever livenessProbe configuration you want.
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/
##
livenessProbe:
  httpGet:
    path: /healthz
    port: http
  initialDelaySeconds: 10
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 2
  successThreshold: 1
## Readiness Probe. The block is directly forwarded into the deployment, so you can use whatever readinessProbe configuration you want.
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/
##
readinessProbe:
  httpGet:
    path: /healthz
    port: http
  initialDelaySeconds: 5
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 6
  successThreshold: 1

## Configure extra volumes
extraVolumes: []

## Configure extra volumeMounts
extraVolumeMounts: []

## Prometheus Exporter / Metrics
##
metrics:
  enabled: true
  ## Metrics exporter pod Annotation and Labels
  ##
  podAnnotations:
    prometheus.io/scrape: "true"
    prometheus.io/port: "7979"

  ## Prometheus Operator ServiceMonitor configuration
  ##
  serviceMonitor:
    enabled: false
    ## Namespace in which Prometheus is running
    ##
    # namespace: monitoring

    ## Interval at which metrics should be scraped.
    ## ref: https://github.com/coreos/prometheus-operator/blob/master/Documentation/api.md#endpoint
    ##
    # interval: 10s

    ## Timeout after which the scrape is ended
    ## ref: https://github.com/coreos/prometheus-operator/blob/master/Documentation/api.md#endpoint
    ##
    # scrapeTimeout: 10s

    ## ServiceMonitor selector labels
    ## ref: https://github.com/bitnami/charts/tree/master/bitnami/prometheus-operator#prometheus-configuration
    ##
    # selector:
    #   prometheus: my-prometheus