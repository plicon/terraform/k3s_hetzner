
provider "kubernetes" {
  host              = var.rancher_api_server_url
  token             = var.rancher_api_token
  insecure          = true
}