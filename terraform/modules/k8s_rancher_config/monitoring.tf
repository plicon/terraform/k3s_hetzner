######### MONITORING
#resource "rancher2_namespace" "monitoring" {
#  name       = "monitoring"
#  project_id = rancher2_project.infra.id
#}
#
#resource "rancher2_app_v2" "monitoring" {
#  cluster_id    = var.cluster_id
#  project_id    = rancher2_project.infra.id
#  name          = "monitoring"
#  namespace     = rancher2_namespace.monitoring.id
#  repo_name     = "rancher-charts"
#  chart_name    = "rancher-monitoring"
#  chart_version = "9.4.201"
#  values        = file("${path.module}/values/monitoring_values.yaml")
#}