#---------------------------------------------------------------------
# RANCHER APP INSTALLATION/CONFIGURATION
#---------------------------------------------------------------------
# Create hosting project
resource "rancher2_project" "hosting" {
  name                      = "Hosting"
  cluster_id                = var.cluster_id
}
resource "rancher2_project" "infra" {
  name                      = "Infra"
  cluster_id                = var.cluster_id
}

## Add Catalog
resource "rancher2_catalog_v2" "bitnami" {
  name        = "bitnami"
  url         = "https://charts.bitnami.com/bitnami"
  cluster_id  = var.cluster_id
}
resource "rancher2_catalog_v2" "ingress-nginx" {
  name        = "ingress-nginx"
  url         = "https://kubernetes.github.io/ingress-nginx"
  cluster_id  = var.cluster_id
}

resource "rancher2_catalog_v2" "nginx-stable" {
  name        = "nginx-ingress"
  url         = "https://helm.nginx.com/stable"
  cluster_id  = var.cluster_id
}
resource "rancher2_catalog_v2" "jetstack" {
  name        = "jetstack"
  url         = "https://charts.jetstack.io"
  cluster_id  = var.cluster_id
}
resource "rancher2_catalog_v2" "argocd" {
  name        = "argo"
  url         = "https://argoproj.github.io/argo-helm"
  cluster_id  = var.cluster_id
}