#resource "rancher2_namespace" "mattermost" {
#  name       = "mattermost"
#  project_id = rancher2_project.infra.id
#}

resource "rancher2_catalog_v2" "mattermost" {
  name        = "mattermost"
  url         = "https://helm.mattermost.com"
  cluster_id  = var.cluster_id
}
resource "rancher2_catalog_v2" "incubator" {
  name        = "incubator"
  url         = "https://charts.helm.sh/incubator"
  cluster_id  = var.cluster_id
}

resource "rancher2_app_v2" "mattermost" {
  depends_on    = [kubernetes_storage_class.delete]
  cluster_id    = var.cluster_id
  project_id    = rancher2_project.infra.id
  name          = "mattermost-enterprise-edition"
  #namespace     = rancher2_namespace.mattermost.id
  namespace     = "mattermost"
  repo_name     = "mattermost"
  chart_name    = "mattermost-enterprise-edition"
  chart_version = "1.16.0"
  wait          = true
  values        = file("${path.module}/values/mattermost_values.yaml")
}

### Create DNS record for mattermost
resource "cloudflare_record" "mattermost" {
  zone_id = var.cloudflare_zone_id
  name    = "${var.mattermost_subdomain}.${var.ceph_domain}"
  value   = "${var.cf_value}.${var.mattermost_domain}"
  type    = "CNAME"
  proxied = true
}

# Create acess application and policy
resource "cloudflare_access_application" "mattermost-access" {
  zone_id                   = var.cloudflare_zone_id
  name                      = "Rook-Ceph Dashboard Application"
  domain                    = "${var.mattermost_subdomain}.${var.mattermost_domain}"
  type                      = "self_hosted"
  session_duration          = "8h"
  auto_redirect_to_identity = false
}

# Allowing access to `@plicon.nl` email address only
resource "cloudflare_access_policy" "mattermost-access" {
  application_id = cloudflare_access_application.mattermost-access.id
  zone_id        = var.cloudflare_zone_id
  name           = "Rook-Ceph Dashboard policy"
  precedence     = "1"
  decision       = "allow"

  include {
    email_domain = [ var.mattermost_domain ]
  }

  require {
    email_domain = [ var.mattermost_domain ]
  }
}
