# Set the variable value in *.tfvars file
#------------------------------------------------------
# GENERAL
#------------------------------------------------------
variable "cluster_id" {}
variable "kube_config" {}
variable "rancher_api_server_url" {}
variable "rancher_api_token" {}

variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}

variable "cloudflare_zone_id" {}
variable "ceph_subdomain" {}
variable "ceph_domain" {}
variable "cf_value" {}

variable "mattermost_subdomain" {}
variable "mattermost_domain" {}