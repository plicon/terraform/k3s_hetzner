######## ROOK-CEPH
#
# See: https://github.com/rook/rook/blob/master/Documentation/ceph-quickstart.md
#
# See: https://github.com/rook/rook/blob/master/Documentation/ceph-dashboard.md
# dashboard access:
# kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o jsonpath="{['data']['password']}" | base64 --decode && echo
#
#########
resource "rancher2_catalog_v2" "rook-cluster" {
  name        = "rook-ceph-cluster"
  url         = "https://charts.rook.io/master"
  cluster_id  = var.cluster_id
}
resource "rancher2_catalog_v2" "rook-operator" {
  name        = "rook-ceph"
  url         = "https://charts.rook.io/release"
  cluster_id  = var.cluster_id
}
resource "rancher2_namespace" "rook-ceph" {
  name       = "rook-ceph"
  project_id = rancher2_project.infra.id
}

resource "rancher2_app_v2" "rook-ceph" {
  cluster_id    = var.cluster_id
  project_id    = rancher2_project.infra.id
  name          = "rook-ceph"
  namespace     = rancher2_namespace.rook-ceph.id
  repo_name     = "rook-ceph"
  chart_name    = "rook-ceph"
  chart_version = "1.6.7"
  wait          = true
  values        = file("${path.module}/values/rook-ceph_values.yaml")
}

resource "rancher2_app_v2" "rook-ceph-cluster" {
  depends_on    = [
    rancher2_app_v2.rook-ceph,
  ]
  cluster_id    = var.cluster_id
  project_id    = rancher2_project.infra.id
  name          = "rook-ceph-cluster"
  namespace     = rancher2_namespace.rook-ceph.id
  repo_name     = "rook-ceph-cluster"
  chart_name    = "rook-ceph-cluster"
  chart_version = "0"
  wait          = true
  values        = file("${path.module}/values/rook-ceph-cluster_values.yaml")
}

resource "kubernetes_secret" "rook-ceph-dashboard" {
  depends_on = [rancher2_app_v2.rook-ceph-cluster]
  metadata {
    name      = "rook-ceph-dashboard-password"
    namespace = rancher2_namespace.rook-ceph.id
  }

  data = {
    password = "Mh*Q-FYxxZrv4TC8ZKQxtXnUx8"
  }
  type = "kubernetes.io/rook"
}

#Create storageclasses
resource "kubernetes_storage_class" "delete" {
  depends_on = [rancher2_app_v2.rook-ceph-cluster]
  metadata {
    annotations = {
      "storageclass.beta.kubernetes.io/is-default-class" = "true"
      "storageclass.kubernetes.io/is-default-class" = "true"
    }
    name = "rook-cephfs"
  }
  storage_provisioner = "rook-ceph.cephfs.csi.ceph.com"
  reclaim_policy      = "Delete"
  parameters = {
    # clusterID is the namespace where operator is deployed.
    "clusterID": rancher2_namespace.rook-ceph.id
    # CephFS filesystem name into which the volume shall be created
    "fsName": "myfs"
    # Ceph pool into which the volume shall be created
    # Required for provisionVolume": "true"
    "pool": "myfs-data0"
    # The secrets contain Ceph admin credentials. These are generated automatically by the operator
    # in the same namespace as the cluster.
    "csi.storage.k8s.io/provisioner-secret-name": "rook-csi-cephfs-provisioner"
    "csi.storage.k8s.io/provisioner-secret-namespace": rancher2_namespace.rook-ceph.id
    "csi.storage.k8s.io/controller-expand-secret-name": "rook-csi-cephfs-provisioner"
    "csi.storage.k8s.io/controller-expand-secret-namespace": rancher2_namespace.rook-ceph.id
    "csi.storage.k8s.io/node-stage-secret-name": "rook-csi-cephfs-node"
    "csi.storage.k8s.io/node-stage-secret-namespace": rancher2_namespace.rook-ceph.id
  }
}
#### Create FS
resource "null_resource" "cephfs"{
  depends_on = [kubernetes_storage_class.delete]
  provisioner "local-exec" {
    command = "kubectl --kubeconfig ${var.kube_config} apply -f ${path.module}/manifests/cephfs.yaml"
  }
}
resource "null_resource" "ceph-toolbox"{
  depends_on = [kubernetes_storage_class.delete]
  provisioner "local-exec" {
    command = "kubectl --kubeconfig ${var.kube_config} apply -f ${path.module}/manifests/rook-toolbox.yaml"
  }
}

# Dashboard for monitoring etc
#
resource "kubernetes_ingress" "ceph-dashboard" {
  metadata {
    name = "rook-ceph-mgr-dashboard"
    namespace     = rancher2_namespace.rook-ceph.id
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
      "nginx.ingress.kubernetes.io/backend-protocol" = "HTTP"
    }
  }
  spec {
    rule {
      host = "${var.ceph_subdomain}.${var.ceph_domain}"
      http {
        path {
          backend {
            service_name = "rook-ceph-mgr-dashboard"
            service_port = "http-dashboard"
          }
        }
      }
    }
  }
}

# Create DNS record for ceph dashboard
resource "cloudflare_record" "ceph-dashboard" {
  zone_id = var.cloudflare_zone_id
  name    = "${var.ceph_subdomain}.${var.ceph_domain}"
  value   = "${var.cf_value}.${var.ceph_domain}"
  type    = "CNAME"
  proxied = true
}

# Create acess application and policy
resource "cloudflare_access_application" "ceph-access" {
  zone_id                   = var.cloudflare_zone_id
  name                      = "Rook-Ceph Dashboard Application"
  domain                    = "${var.ceph_subdomain}.${var.ceph_domain}"
  type                      = "self_hosted"
  session_duration          = "8h"
  auto_redirect_to_identity = false
}

# Allowing access to `@plicon.nl` email address only
resource "cloudflare_access_policy" "ceph-access" {
  application_id = cloudflare_access_application.ceph-access.id
  zone_id        = var.cloudflare_zone_id
  name           = "Rook-Ceph Dashboard policy"
  precedence     = "1"
  decision       = "allow"

  include {
    email_domain = [ var.ceph_domain ]
  }

  require {
    email_domain = [ var.ceph_domain ]
  }
}
