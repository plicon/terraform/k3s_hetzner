terraform {
  backend "s3" {
    # Replace this with your bucket name!
    shared_credentials_file = "/Users/patrick/.aws/credentials"
    profile                 = "plicon"
    bucket                  = "aws-plicon-tf-k8s-states"
    key                     = "eu-central-1/s3/hetzner_cloud_k3s_dev/terraform_apps.tfstate"
    region                  = "eu-central-1"
    # Replace this with your DynamoDB table name!
    dynamodb_table          = "aws-plicon-tf-k8s-locks"
    encrypt                 = true
  }
}

data "terraform_remote_state" "cluster" {
  backend = "s3"

  config  = {
    shared_credentials_file = "/Users/patrick/.aws/credentials"
    profile                 = "plicon"
    bucket                  = "aws-plicon-tf-k8s-states"
    key                     = "eu-central-1/s3/hetzner_cloud_k3s_dev/terraform.tfstate"
    region                  = "eu-central-1"
    dynamodb_table          = "aws-plicon-tf-k8s-locks"
    encrypt                 = true
  }
}