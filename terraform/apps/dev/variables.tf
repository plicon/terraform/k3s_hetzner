#------------------------------------------------------
# GENERAL GETZBER_K3S_MODULE
#------------------------------------------------------

# or using the -var="hcloud_token=..." CLI option
variable "environment" {
  default = "my_env"
}
variable "cluster" {
  default = "my_cluster"
}
variable "subdomain" {
  default = "my"
}
variable "domain" {
  default = "domain.nl"
}
variable "custom_tags" {
  type = map
  default = {
    "Application" = "k3s"
    "Environment" = "my_env"
  }
}

#------------------------------------------------------
# CLOUDFLARE DOMAIN components
#------------------------------------------------------
variable "cloudflare_email" {
  default = "mail@domain.nl"
}
variable "cloudflare_api_token" {
  default = "my_super_secret_api_token"
}
variable "cloudflare_zone_id" {
  default = "1234567890987654321"
}

#------------------------------------------------------
# GENERAL K8S_RANCHER MODULE
#------------------------------------------------------
variable "kube_config" {
  default = "~/.kube/config"
}
}