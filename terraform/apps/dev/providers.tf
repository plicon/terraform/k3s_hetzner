
# Configure the cloudflare provider
provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

provider "helm" {
  kubernetes {
    config_path       = pathexpand(data.terraform_remote_state.cluster.outputs.kube_config)
    load_config_file  = true
  }
}

provider "kubernetes" {
  config_path       = pathexpand(data.terraform_remote_state.cluster.outputs.kube_config)
  load_config_file  = true
}

provider "rancher2" {
  api_url   = rancher2_bootstrap.admin.url
  token_key = rancher2_bootstrap.admin.token
  insecure  = true
}