#--------------------------------------------------------------
# General
#--------------------------------------------------------------
environment = "plicon"
cluster     = "k8s-prod"
subdomain   = "k8s.hcl"
domain      = "plicon.nl"

custom_tags = {
  "Application" = "k8s"
  "Environment" = "production"
}


#--------------------------------------------------------------
# Ansible
#--------------------------------------------------------------
ansible_playbook      = "playbook_bootstrap-wireguard.yaml"
ansible_extra_opts    = "--vault-password-file ~/.ansible_secrets/ansible_vault_plicon_k3s_hetzner.passwd"
ansible_extra_vars    = "subdomain=k8s.hcl domain=plicon.nl"
ansible_key_name      = "terraform-ansible-public-key"
ssh_key_public_path   = "~/.ssh/awx_pub_key.pub"
ssh_key_private_path  = "~/.ssh/awx_pub_key"

#--------------------------------------------------------------
# K8S
#--------------------------------------------------------------
image_name    = "ubuntu-18.04"
location      = "nbg1"
kube_config   = "~/.kube/k8s-prod"

# Define amount of hosts
num_master    = "1"
num_worker    = "2"
# Define type of instances
master_type   = "cpx31"
worker_type   = "cpx31"
# Networking
network       = "k8s_network"
network_zone  = "eu-central"
ip_range      = "10.0.0.0/8"
ip_subnet     = "10.0.10.0/24"
# RKE settings
cni_plugin    = "canal"
cni_interface = "enp7s0"