terraform {
  backend "s3" {
    # Replace this with your bucket name!
    shared_credentials_file = "/Users/patrick/.aws/credentials"
    profile                 = "plicon"
    bucket                  = "aws-plicon-tf-k8s-states"
    key                     = "eu-central-1/s3/hetzner_cloud_k8s_prod/terraform.tfstate"
    region                  = "eu-central-1"
    # Replace this with your DynamoDB table name!
    dynamodb_table          = "aws-plicon-tf-k8s-locks"
    encrypt                 = true
  }
}

module "hetzner_k8s_cluster" {
  source = "../../modules/hetzner_rke_cluster"
  #################
  # Define module variables as defined in tfvars files
  environment                   = var.environment
  cluster                       = var.cluster
  subdomain                     = var.subdomain
  domain                        = var.domain
  custom_tags                   = var.custom_tags
  ansible_playbook              = var.ansible_playbook
  ansible_extra_opts            = var.ansible_extra_opts
  ansible_extra_vars            = var.ansible_extra_vars
  ansible_key_name              = var.ansible_key_name
  ssh_key_public_path           = var.ssh_key_public_path
  ssh_key_private_path          = var.ssh_key_private_path
  image_name                    = var.image_name
  location                      = var.location
  kube_config                   = var.kube_config
  num_master                    = var.num_master
  num_worker                    = var.num_worker
  master_type                   = var.master_type
  worker_type                   = var.worker_type
  network                       = var.network
  network_zone                  = var.network_zone
  ip_range                      = var.ip_range
  ip_subnet                     = var.ip_subnet
  cni_plugin                    = var.cni_plugin
  cni_interface                 = var.cni_interface
  hcloud_token                  = var.hcloud_token
  cloudflare_api_token          = var.cloudflare_api_token
  cloudflare_zone_id            = var.cloudflare_zone_id
  aws_access_key_id             = var.aws_access_key_id
  aws_secret_access_key         = var.aws_secret_access_key
}

module "k8s_rancher_apps" {
  source      = "../../modules/k8s_rancher_apps"
  depends_on  = [module.hetzner_k8s_cluster]

  providers   = {
    kubernetes            = kubernetes
  }
  #################
  # Define module variables as defined in tfvars files
  cluster_id                    = module.hetzner_k8s_cluster.cluster_id
  cloudflare_zone_id            = var.cloudflare_zone_id
  cloudflare_argo_cert_key_file = var.cloudflare_argo_cert_key_file
  kube_config                   = var.kube_config
}