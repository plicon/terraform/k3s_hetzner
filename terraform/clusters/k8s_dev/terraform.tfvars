#--------------------------------------------------------------
# General
#--------------------------------------------------------------
environment = "plicon"
cluster     = "k8s-dev"
subdomain   = "dev.hcl"
domain      = "plicon.nl"

custom_tags = {
  "application" = "k8s"
  "environment" = "development"
  "organisation"= "plicon"
}

#--------------------------------------------------------------
# Ansible
#--------------------------------------------------------------
ansible_playbook      = "playbook_bootstrap_hosts.yaml"
ansible_extra_opts    = "--vault-password-file ~/.ansible_secrets/ansible_vault_plicon_k3s_hetzner.passwd"
ansible_extra_vars    = "subdomain=dev.hcl domain=plicon.nl"
ansible_key_name      = "terraform-ansible-public-key"
ssh_key_public_path   = "~/.ssh/awx_pub_key.pub"
ssh_key_private_path  = "~/.ssh/awx_pub_key"

#--------------------------------------------------------------
# K8S
#--------------------------------------------------------------
image_name    = "ubuntu-18.04"
location      = "nbg1"
kube_config   = "~/.kube/k8s-dev"

# Define amount of hosts
num_master    = "1"
num_worker    = "3"
# Define volume size
volume_master = 0
volume_worker = 25
# Define type of instances
master_type   = "cx31"
worker_type   = "cx31"
# Networking
network       = "backend"
network_zone  = "eu-central"
ip_range      = "10.0.0.0/8"
ip_subnet     = "10.0.10.0/24"
# RKE settings
#cni_plugin    = "calico"
cni_plugin    = "canal"
#cni_interface = "enp7s0"
# Use interface "enp7s0" for  cpxXX host type
cni_interface = "ens10"
# Use interface "ens10" for  cxXX host type


#--------------------------------------------------------------
# CLOUDFLARE
#--------------------------------------------------------------
cloudflare_argo_tunnel_host = "k8s-hcl-dev"