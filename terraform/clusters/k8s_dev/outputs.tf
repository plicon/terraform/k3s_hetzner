output "subdomain" {
  value = var.subdomain
}
output "domain" {
  value = var.domain
}
output "kube_config" {
  value = var.kube_config
}