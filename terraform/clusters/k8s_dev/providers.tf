
# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}
# Configure the cloudflare provider
provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

provider "rancher2" {
  api_url    = var.rancher_api_url
  access_key = var.rancher_access_key
  secret_key = var.rancher_secret_key
  insecure   = true
}