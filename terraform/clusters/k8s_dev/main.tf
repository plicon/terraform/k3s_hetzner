terraform {
  backend "s3" {
    # Replace this with your bucket name!
    shared_credentials_file = "/Users/patrick/.aws/credentials"
    profile                 = "plicon"
    bucket                  = "aws-plicon-tf-k8s-states"
    key                     = "eu-central-1/s3/hetzner_cloud_k8s_dev/terraform.tfstate"
    region                  = "eu-central-1"
    # Replace this with your DynamoDB table name!
    dynamodb_table          = "aws-plicon-tf-k8s-locks"
    encrypt                 = true
  }
}

# Define local variables
module "hetzner_rke_cluster" {
  source = "../../modules/hetzner_rke_cluster"
  #################
  # Define module variables as defined in tfvars files
  environment                   = var.environment
  cluster                       = var.cluster
  subdomain                     = var.subdomain
  domain                        = var.domain
  custom_tags                   = var.custom_tags
  ansible_playbook              = var.ansible_playbook
  ansible_extra_opts            = var.ansible_extra_opts
  ansible_extra_vars            = var.ansible_extra_vars
  ansible_key_name              = var.ansible_key_name
  ssh_key_public_path           = var.ssh_key_public_path
  ssh_key_private_path          = var.ssh_key_private_path
  image_name                    = var.image_name
  location                      = var.location
  kube_config                   = var.kube_config
  num_master                    = var.num_master
  num_worker                    = var.num_worker
  volume_worker                 = "25"
  master_type                   = var.master_type
  worker_type                   = var.worker_type
  network                       = var.network
  network_zone                  = var.network_zone
  ip_range                      = var.ip_range
  ip_subnet                     = var.ip_subnet
  cni_plugin                    = var.cni_plugin
  cni_interface                 = var.cni_interface
  network_policy                = var.network_policy
  hcloud_token                  = var.hcloud_token
  cloudflare_api_token          = var.cloudflare_api_token
  cloudflare_zone_id            = var.cloudflare_zone_id
  aws_access_key_id             = var.aws_access_key_id
  aws_secret_access_key         = var.aws_secret_access_key
}
module "k8s_rancher_config" {
  source                  = "../../modules/k8s_rancher_config"
  cluster_id              = module.hetzner_rke_cluster.cluster_id
  kube_config             = var.kube_config
  rancher_api_server_url  = module.hetzner_rke_cluster.rancher_api_server_url
  rancher_api_token       = module.hetzner_rke_cluster.rancher_api_token
  aws_access_key_id       = var.aws_access_key_id
  aws_secret_access_key   = var.aws_secret_access_key
  ceph_subdomain          = "ceph-dev"
  ceph_domain             = var.domain
  mattermost_subdomain    = "mattermost-dev"
  mattermost_domain       = var.domain
  cloudflare_zone_id      = var.cloudflare_zone_id
  cf_value                = var.cloudflare_argo_tunnel_host

}

module "k8s_rancher_ingress-nginx" {
  source          = "../../modules/k8s_rancher_ingress-nginx"
  cluster_id      = module.hetzner_rke_cluster.cluster_id
  project_id      = module.k8s_rancher_config.hosting_project_id
  cf_argo_tunnel  = var.cloudflare_argo_tunnel_host
  domain          = var.domain
}

module "k8s_argocd" {
  source                  = "../../modules/k8s_argocd"
  cluster_id              = module.hetzner_rke_cluster.cluster_id
  project_id              = module.k8s_rancher_config.hosting_project_id
  argo_admin_bcrypted     = var.argo_admin_bcrypted
  argo_target_revision    = lookup(var.custom_tags, "environment")
  gitlab_privatekey       = var.gitlab_privatekey
  argo_subdomain          = "argocd-dev"
  argo_domain             = var.domain
  cloudflare_zone_id      = var.cloudflare_zone_id
  cf_value                = var.cloudflare_argo_tunnel_host

  depends_on              = [module.k8s_rancher_ingress-nginx]
}