# K3S Cluster @Hetzner

This repository contains a terraform manifest for creating and installing a k3s cluster running Rancher on Hetzner cloudprovider

Also see https://community.hetzner.com/tutorials/install-kubernetes-cluster for more information

## Requirements

Ansible uses dynamic inventory which required hcloud-python package
```bash
pip install hcloud
```

Using the following resources @Hetzner
* 1x cloud network
* 1x CX11 machine as k3s master
* 2x CX31 machine as k3s workers
* 1 IPv4 floating IP (for loadbalancing services)

# WIP
* set password for root
* cloudflare argo tunnel as sidecar with traefik
* traefik



# TODO
* setup resources for k3s cluster (ansible?)
  * storage
  * firewall
  * ingress (nginx/traefik)
  * metallb
  * cloudflare argo tunnel
* install apps
  * awx
  * hashicorp consul / vault
  * hue-api-shizzle
  * kube chaos killing

# DONE
* terraform state stored in s3
* create resource on hcloud
* configure DNS for nodes (cloudflare)
* set password for patrick && add patrick to sudoers
* Setup k3s cluster with ansible
* add cluster to rancher
* Configure cluster with hcloud CCM
* Install / COnfigure
  * monitoring
  * longhorn



## KILO notes

```bash
for node in $(kubectl get nodes | grep -i k8s | awk '{print $1}'); do kubectl annotate node $node kilo.squat.ai/location="hetzner"; done

kubectl apply -f https://raw.githubusercontent.com/squat/kilo/master/manifests/kilo-k3s-flannel.yaml
```
apiVersion: kilo.squat.ai/v1alpha1
kind: Peer
metadata:
  name: knieptang
spec:
  allowedIPs:
  - 10.5.0.1/32
  - 172.16.0.0/29
  - 192.168.1.0/24
  persistentKeepalive: 25
  presharedKey: hGShrx3VIKesWnAA93mrdOIBTpchls074CvqVWD/4y0=
  publicKey: 9FaAjEDTM0RmMJdfRuZyWevji0uVoBq4QJhq/FeJOHc=

apiVersion: kilo.squat.ai/v1alpha1
kind: Peer
metadata:
  name: macbook
spec:
  allowedIPs:
  - 172.16.0.1/32
  - 192.168.1.0/24
  - 192.168.30.0/24
  publicKey: "{{ kilo_pubkey }}
  presharedKey:
  persistentKeepalive: 25


  ## RANCHER SETUP

  ```bash
  kubectl create namespace cert-manager
  kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.0/cert-manager.yaml


  helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
  helm repo update;
  kubectl create namespace cattle-system
  helm install rancher rancher-stable/rancher \
  --namespace cattle-system \
  --set hostname=rancher.hcloud.plicon.nl \
  --set ingress.tls.source=letsEncrypt \
  --set letsEncrypt.email=patrick@plicon.nl

  # Check status
  kubectl -n cattle-system rollout status deploy/rancher
  ```

## RANCHER NOTES
* Reset admin password
```bash
export KUBECONFIG=~/.kube/hetzner_cloud
   kubect l--kubeconfig $KUBECONFIG -n cattle-system exec $(kubectl --kubeconfig $KUBECONFIG -n cattle-system get pods -l app=rancher | grep '1/1' | head -1 | awk '{ print $1 }') -- reset-password
```